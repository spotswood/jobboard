<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = ['title', 'description', 'email', 'status'];

    protected $attributes = array(
        'status' => 'pending',
    );

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
