<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCreationHistory extends Model
{
    protected $fillable = [
        'job_id', 'job_title', 'job_creator', 'job_creator_email'
    ];

    public function contains(User $user)
    {

    }
}
