<?php

namespace App\Listeners;

use App\Events\FirstJobWasCreated;
use App\JobCreationHistory;
use App\Role;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class EmailJobModeratorsAboutFirstJobOfUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FirstJobWasCreated  $event
     * @return void
     */
    public function handle(FirstJobWasCreated $event)
    {
        $data = [];

        $moderatorRoleId = Role::where('name', 'jobmoderator')->first()->id;
        $moderators = User::where('role_id', $moderatorRoleId)->get();

        $numberOfModerators = count($moderators);
        $data['countmoderators'] = $numberOfModerators;
        $data['job'] = $event->job;

        foreach ($moderators as $moderator) {
            Mail::send('emails.firstjobmoderators', $data, function ($message) use ($moderator) {
                $message->from('jobs@piratetech.com', 'PirateJobs Testapp');
                $message->subject('A user posted his first job');
                $message->to($moderator->email);
            });
        }
    }
}
