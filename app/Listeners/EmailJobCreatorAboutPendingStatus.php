<?php

namespace App\Listeners;

use App\Events\FirstJobWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class EmailJobCreatorAboutPendingStatus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FirstJobWasCreated  $event
     * @return void
     */
    public function handle(FirstJobWasCreated $event)
    {
        $job = $event->job;
        $user = $job->user;

        $data = [];

        $data['job'] = $event->job;
        $data['user'] = $user;

        Mail::send('emails.firstjobcreator', $data, function ($message) use ($user) {
            $message->from('jobs@piratetech.com', 'PirateJobs Testapp');
            $message->subject('First job created, but still in moderation');
            $message->to($user->email);
        });
    }
}
