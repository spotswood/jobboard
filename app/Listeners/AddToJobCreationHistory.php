<?php

namespace App\Listeners;

use App\Events\FirstJobWasCreated;
use App\Events\JobWasCreated;
use App\JobCreationHistory;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Event;

class AddToJobCreationHistory
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobWasCreated  $event
     * @return void
     */
    public function handle(JobWasCreated $event)
    {
        $historyRow = new JobCreationHistory();

        $historyRow->fill([
            'job_id' => $event->job->id,
            'job_creator' => $event->job->user->name,
            'job_title' => $event->job->title,
            'job_creator_email' => $event->job->user->email
        ]);

        $historyRow->save();

        //check if user has jobs, if not, fire FirstJobWasCreated
        $user = $event->job->user;

        $numberOfJobsByUser = count(JobCreationHistory::where('job_creator_email', $user->email)->get());

        if ($numberOfJobsByUser == 1) {
            Event::fire(new FirstJobWasCreated($event->job));
            flash()->success('You have created your first job. It is not yet published, check email for more info.');
        } else {
            $event->job->status = 'published';
            $event->job->save();
        }
    }
}
