<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function jobs()
    {
        return $this->hasMany('App\Job');
    }

    public function is($role)
    {
        if ($this->role->name == $role) return true;
        return false;
    }

    public function owns($model)
    {
        if ($this->id == $model->user_id) {
            return true;
        }
        return false;
    }



}
