<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
        $gate->define('create-job', function($user){
            if ($user->is('hrmanager')) {
                return true;
            }
            return false;
        });

        $gate->define('edit-job', function($user, $job){
            if ($user->owns($job)) {
                return true;
            }
            return false;
        });

        $gate->define('moderate-job', function($user){
            if ($user->is('jobmoderator')) {
                return true;
            }
            return false;
        });

        $gate->define('publish-job', function($user){
            if ($user->is('jobmoderator')) {
                return true;
            }
            return false;
        });
    }
}
