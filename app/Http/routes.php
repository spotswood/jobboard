<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect(route('jobs.index'));
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');

    Route::resource('jobs', 'JobsController');

    Route::patch('/jobs/{id}/status/{status}', [
        'as' => 'jobs.status',
        'uses' => 'JobsController@setStatus'
    ]);

    Route::get('/jobs/{id}/moderate', [
        'as' => 'jobs.moderate',
        'uses' => 'JobsController@moderate'
    ]);

    Route::get('/jobs/{id}/status/{status}', [
//        'middleware' => 'role:jobmoderator',
        'as' => 'jobs.moderate.status',
        'uses' => 'JobsController@setStatus'
    ]);


    Route::get('testmail', [
        'as' => 'testmail', 'uses' => 'HomeController@testmail'
    ]);

    Route::get('test', [
        'as' => 'test', 'uses' => 'HomeController@test'
    ]);
});
