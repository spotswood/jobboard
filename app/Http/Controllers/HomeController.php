<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\JobCreationHistory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function testmail()
    {
        $data = [];

        Mail::send('emails.testmail', $data, function ($message) {
            $message->from('jobs@board.com', 'Job Board app');

            $message->to('neb.vojvodic@gmail.com');
        });

        $user = User::findOrFail(1);

        return $user;
    }

    public function test()
    {
        return count(JobCreationHistory::where('job_creator_email', 'neb.vojvodic@gmail.com')->get());
    }
}
