<?php

namespace App\Http\Controllers;

use App\Events\JobWasCreated;
use App\Http\Requests\CreateJobRequest;
use App\Http\Requests\SetJobStatusRequest;
use App\Job;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Symfony\Component\HttpFoundation\Response;

class JobsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role', [['hrmanager'], 'only' => 'setstatus, moderate']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $jobs = Job::all();
        } else {
            $jobs = Job::where('status', 'published')->get();
        }

        return view('jobs.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->cannot('create-job')) {
            return redirect(route('jobs.index'));
        }

        return view('jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateJobRequest $request)
    {
        $user = Auth::user();

        $job = Job::create($request->all());

        $job->status = 'pending';

        $user->jobs()->save($job);

        flash()->success('You have created a new job.');

        Event::fire(new JobWasCreated($job));

        return redirect(route('jobs.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job = Job::findOrFail($id);

        if ($job->status == 'pending' && Auth::guest()) {
            return redirect(route('jobs.index'));
        }

        return view('jobs.show', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::findOrFail($id);

        if (Auth::user()->cannot('edit-job', $job)) {
            return redirect(route('jobs.index'));
        }

        return view('jobs.edit', compact('job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateJobRequest $request, $id)
    {
        $job = Job::findOrFail($id);

        if (Auth::user()->cannot('edit-job', $job)) {
            return redirect(route('jobs.index'));
        }

        $job->title = $request->get('title');
        $job->description = $request->get('description');
        $job->email = $request->get('email');

        $job->save();
//        $job = Job::updateOrCreate($request->all());

        return redirect(route('jobs.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setStatus(Request $request, $id, $status)
    {

        $job = Job::findOrFail($id);

        $job->status = $status;
        $job->save();

        if ($request->ajax()) {
            return $job;
        }

        flash()->info('Job ' . $job->title . ' is now ' . $job->status);

        return redirect(route('jobs.index'));
    }

    public function moderate(Request $request, $id)
    {
        $job = Job::findOrFail($id);

        return view('jobs.moderate', compact('job'));
    }
}
