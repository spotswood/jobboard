<p>First job was created by user {{ $job->user->name }} ({{ $job->user->email }})</p>
<h3>Job title: {{ $job->title }}</h3>
<p>
    As you can <a href="{{ route('jobs.show', $job->id) }}">View this job</a>
</p>
<p>
    or make Direct actions:<br>
    <a href="{{ route('jobs.moderate.status', array($job->id, 'published')) }}">Publish this job</a> or <br>
    <a href="{{ route('jobs.moderate.status', array($job->id, 'spam')) }}">Mark job as spam</a>
</p>