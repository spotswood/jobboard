<p>Hello {{ $job->user->name }} ({{ $job->user->email }}),</p>
<h3>You have created your first job: {{ $job->title }}</h3>
<p>
    However, it is <strong>not yet published</strong>.
</p>
<p>
    All moderators have been notified by email, and only they can publish it.
</p>
<p>
    You may <a href="{{ route('jobs.show', $job->id) }}">view your job</a>.
</p>