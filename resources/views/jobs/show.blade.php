@extends('layouts.app')

@section('addHead')
    {{--<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>--}}
    {{--<script>tinymce.init({ selector:'textarea' });</script>--}}
@stop

@section('content')
    <div class="container">
        <div class="page-header">
            <h1>Job posting</h1>
        </div>

        <a href="{{ route('jobs.index') }}" class="btn btn-default">All jobs</a>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Job posting</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('jobs.update', $job->id) }}">
                            <input type="hidden" name="_method" value="PUT">
                            {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    {{--<input type="text" class="form-control" name="title" value="{{ $job->title }}">--}}
                                    <div class="well">{{ $job->title }}</div>

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                        <div class="well">
                                            {!! $job->description !!}
                                        </div>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-mail</label>

                                <div class="col-md-6">
                                    {{--<input type="email" class="form-control" name="email" value="{{ $job->email }}">--}}
                                    <div class="well">{{ $job->email }}</div>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    @can('edit-job', $job)
                                    <a href="{{ route('jobs.edit', $job->id) }}" class="btn btn-primary">
                                        <i class="fa fa-btn fa-eraser"></i>Edit
                                    </a>
                                    @endcan

                                    @section('moderator-options')
                                    @can('moderate-job')
                                    <a href="{{ route('jobs.moderate', $job->id) }}" class="btn btn-primary">
                                        <i class="fa fa-btn fa-eraser"></i>Moderate
                                    </a>
                                    @endcan
                                    @stop
                                    @yield('moderator-options')

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
