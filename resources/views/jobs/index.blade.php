@extends('layouts.app')

@section('addHead')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@stop

@section('content')
<div class="container">
    <div class="page-header">
        <h1>Jobs</h1>
    </div>

    @can('create-job')
    <p>
        <a href="{{ route('jobs.create') }}" class="btn btn-primary">Create</a>
    </p>
    @endcan

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Title</th>
                        <th>Created</th>
                        <th>By</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($jobs as $job)
                        @if($job->status == 'published')
                            <tr class="success" data-job-id="{{ $job->id }}">
                        @elseif($job->status == 'spam')
                            <tr class="danger" data-job-id="{{ $job->id }}">
                        @else
                            <tr data-job-id="{{ $job->id }}">
                        @endif
                            <td>{{ $job->id }}</td>
                            <td><a href="{{ route('jobs.show', $job->id) }}">{{ $job->title }}</a></td>
                            <td>{{ $job->created_at }}</td>
                            <td>{{ $job->user->name }}</td>
                            <td>{{ $job->status }}</td>
                            <td>
                                @can('edit-job', $job)
                                <a href="{{ route('jobs.edit', $job->id) }}" class="btn btn-default">Edit</a>
                                @endcan

                                @can('publish-job')
                                {{--<a href="{{ route('jobs.edit', $job->id) }}" class="btn btn-default">Set status</a>--}}
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        Set status
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" data-job-id="{{ $job->id }}">
                                        <li><a href="#" data-job-status="pending"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Pending</a></li>
                                        <li><a href="#" data-job-status="published"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Publish</a></li>
                                        <li><a href="#" data-job-status="spam"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Spam</a></li>
                                        {{--<li role="separator" class="divider"></li>--}}
                                        {{--<li><a href="#" data-job-status="delete"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></li>--}}
                                    </ul>
                                </div>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>No jobs.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    @endforelse
                </tbody>
                {{--<tr class="active">--}}

                {{--</tr>--}}
                {{--<tr class="success">...</tr>--}}
                {{--<tr class="warning">...</tr>--}}
                {{--<tr class="danger">...</tr>--}}
                {{--<tr class="info">...</tr>--}}
            </table>
        </div>
    </div>
</div>
@stop

@section('addJs')
    <script>
        $.ajaxSetup({
            url: "ping.php"
        });

        $('tbody a[data-job-status]').on( "click", function() {
            var status = $(this).attr('data-job-status');
            var ulelement = $(this).closest('[data-job-id]', $(this));
            var jobid = ulelement.attr('data-job-id');

            setStatus(jobid, status)
        });

        function setStatus(jobid, jobstatus){

            $.ajax({
                method: "POST",
                url: "jobs/"+jobid+"/status/"+jobstatus,
                data: {
                    _method: "PATCH",
                    jobid: jobid,
                    status: jobstatus
                },
                success: function (data) {
                    console.log(data);
                    updateJobRow(data);
                },
                fail: function (data) {
                    console.log('failed');
                }
            });
        }

        function updateJobRow(job){
            var row = $('tbody').find("tr[data-job-id='" + job.id + "']");
            setClassForJobRow(job, row);
            var statusField = row.find('td:nth-child(5)');
            statusField.text(job.status);
        }

        function setClassForJobRow(job, row){
            row.removeClass('success');
            row.removeClass('danger');

            if (job.status == 'pending') {
                return;
            }
            if (job.status == 'spam') {
                row.addClass('danger');
                return;
            }
            if (job.status == 'published') {
                row.addClass('success');
            }
            return;
        }
    </script>
@stop