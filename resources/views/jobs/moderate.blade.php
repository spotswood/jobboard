@extends('jobs.show')

@section('moderator-options')

    <a href="{{ route('jobs.moderate.status', array($job->id, 'published')) }}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Publish this job</a>
    <a href="{{ route('jobs.moderate.status', array($job->id, 'pending')) }}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Mark as pending</a>
    <a href="{{ route('jobs.moderate.status', array($job->id, 'spam')) }}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Mark as spam</a>
@overwrite