<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobCreationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_creation_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->string('job_title');
            $table->string('job_creator');
            $table->string('job_creator_email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_creation_histories');
    }
}
