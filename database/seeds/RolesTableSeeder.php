<?php

use App\Role;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
//use Laracasts\TestDummy\Factory as TestDummy;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        Role::create([
            'name' => 'hrmanager',
            'text' => 'HR Manager',
        ]);

        Role::create([
            'name' => 'jobmoderator',
            'text' => 'Job Moderator',
        ]);
    }
}
