## Job board app


### Setup in 3 steps

##1. Download Source

Download source or clone this repository locally.

##2. Run composer update

Go to project root and run "composer update". It will download all vendor files needed.

##3. Setup your .env file and mail driver

I am using Mailgun, contact me at neb.vojvodic@gmail.com for Mailgun secret key and a copy of my .env file. Or you can setup mail driver of your choice.